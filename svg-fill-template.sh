#!/bin/bash

# Usage example: ./svg-fill-template.sh -f path/to/template.svg -t {} title="This is the title" content="This is the content" key1="value1" key2="value2" ...

# Inputs:
# $1 = Template path

# Input check
[ $# -eq 0 ] && echo "./svg-fill-template.sh -f path/to/template.svg -t {} title="This is the title" content="This is the content" key1="value1" key2="value2" ..." && exit 1

# Variables
tag="{}"

# https://stackoverflow.com/a/63653944
while [ $# -gt 0 ]; do
  case "$1" in
    -o | --output)
      output="${2}"
      ;;
    -f | --file)
      file="${2}"
      image="$( cat "${2}" )"
      ;;
    -t | --template-tag)
      tag="${2:0:2}"
      ;;
    *=*)
      key_values+="$1\n"
      ;;
    *)
  esac
  shift
done

key_values="$( echo -e "$key_values" )"

# Replace attributes
while IFS= read -r line; do
	image="${image//${tag:0:1}${line%=*}${tag:1:2}/${line#*=}}"
done <<< "$key_values"

echo "$image"

[ -z "$output" ] && flatpak run org.inkscape.Inkscape /dev/stdin <<< "$image" || flatpak run org.inkscape.Inkscape /dev/stdin --export-filename "$output" <<< "$image"
